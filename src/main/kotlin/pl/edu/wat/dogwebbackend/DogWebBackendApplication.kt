package pl.edu.wat.dogwebbackend

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.cloud.openfeign.EnableFeignClients


@EnableFeignClients
@SpringBootApplication
class DogWebBackendApplication

fun main(args: Array<String>) {
    runApplication<DogWebBackendApplication>(*args)
}
