package pl.edu.wat.dogwebbackend.service

import org.slf4j.LoggerFactory
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder
import org.springframework.stereotype.Service
import pl.edu.wat.dogwebbackend.model.DogWebUser
import pl.edu.wat.dogwebbackend.model.Role
import pl.edu.wat.dogwebbackend.model.StringResponse
import pl.edu.wat.dogwebbackend.repository.ShelterRepository
import pl.edu.wat.dogwebbackend.repository.UserRepository
import java.time.LocalDateTime
import java.util.*
import javax.persistence.EntityNotFoundException
import kotlin.random.Random

@Service
class UserService(
        val userRepository: UserRepository,
        val shelterRepository: ShelterRepository,
        val encoder: BCryptPasswordEncoder
) {
    val logger = LoggerFactory.getLogger(UserService::class.java)

    fun addUser(email: String, password: String, shelterid: Long): ResponseEntity<StringResponse> {
        // Sprawdzamy czy user z danymi e-mailem już istnieje, jeśli tak to zwracamy i logujemy error
        val userOptional = userRepository.findByEmail(email)
        if (!userOptional.isPresent) {
            val encodedPassword = encoder.encode(password)
            val shelterOptional = shelterRepository.findById(shelterid)
            // Jeśli schronisko jest w bazie to kontynuujemy rejestrację
            if (shelterOptional.isPresent) {
                with(userRepository) {
                    save(
                            DogWebUser(
                                    password = encodedPassword,
                                    email = email,
                                    createDate = LocalDateTime.now(),
                                    role = Role.ROLE_USER,
                                    shelter = shelterOptional.get(),
                            )
                    )
                }
                logger.info("Udało się stworzyć użytkownika o emailu:$email")
                return ResponseEntity(StringResponse("Udało się utworzyć użytkownika"), HttpStatus.ACCEPTED)

            }
            // Schroniska nie ma w bazie, więc leci error
            logger.warn("Nie ma schroniska o takim id:$shelterid")
            return ResponseEntity(StringResponse("Podane schronisko nie istnieje"), HttpStatus.ACCEPTED)
        }
        // Użytkownika nie ma w bazie, więc leci error
        logger.warn("Użytkownik o podanym adresie e-mail=$email już istnieje")
        return ResponseEntity(StringResponse("Użytkownik o podanym adresie e-mail=$email już istnieje"), HttpStatus.ACCEPTED)
    }


    fun updateUserPassword(email: String, password: String): ResponseEntity<String> {
        val userOptional = userRepository.findByEmail(email)
        if (userOptional.isPresent) {
            val user = userOptional.get()
            with(userRepository) {
                save(
                        DogWebUser(
                                id = user.id,
                                email=email,
                                password = password,
                                createDate = user.createDate,
                                role = user.role,
                                shelter = user.shelter
                        )
                )

            }
            logger.warn("Zmieniono hasło użytkownikowi o adresie e-mail=$email")
            return ResponseEntity("Udało się zmienić hasło", HttpStatus.ACCEPTED)
        }
        logger.warn("Użytkownik o podanym adresie e-mail=$email nie istnieje")
        return ResponseEntity("Użytkownik o podanym adresie e-mail=$email nie istnieje", HttpStatus.NOT_FOUND)
    }

    fun updateUserRole(email: String, role: Role): ResponseEntity<String> {
        val userOptional = userRepository.findByEmail(email)
        if (userOptional.isPresent) {
            val user = userOptional.get()
            with(userRepository) {
                save(
                        DogWebUser(
                                id = user.id,
                                email = email,
                                password = user.password,
                                createDate = user.createDate,
                                role = role,
                                shelter = user.shelter
                        )
                )

            }
            logger.warn("Zmieniono rolę użytkownikowi o adresie e-mail=$email")
            return ResponseEntity("Udało się zmienić Rolę", HttpStatus.ACCEPTED)
        }
        logger.warn("Użytkownik o podanym adresie e-mail=$email nie istnieje")
        return ResponseEntity("Użytkownik o podanym adresie e-mail=$email nie istnieje", HttpStatus.NOT_FOUND)
    }
    fun getUsers() = userRepository.findAll()

    fun getUser(): DogWebUser?{
        val count = userRepository.count()
        if(count<1){
            return null
        }
        val randomId = Random.nextLong(1, count + 1)
        return userRepository.getById(randomId)
    }

//    fun getUserid(id: Long): Optional<DogWebUser> {
//        val userOptional = userRepository.findById(id)
//        if(userOptional.isPresent) {
//            return userRepository.findById(id)
//        }
//        throw Exception("Nie ma takiego ")
//    }


}


