package pl.edu.wat.dogwebbackend.service.test

import pl.edu.wat.dogwebbackend.model.*
import java.time.LocalDateTime

object TestDataProvider {
    fun shelter_paluch() = Shelter(
            name = "Schronisko na Paluchu",
            nip = 233213123,
            description = "Schronisko dla psów",
            createDate = LocalDateTime.now(),
            address = Address(
                    street = "Paluch",
                    city = "Warszawa",
                    postalCode = "02-147",
                    buildingNumber = "2",
                    province = "Mazowieckie",
                    createDate = LocalDateTime.now()
            ),
            contact = Contact(
                    email = "ba@napaluchu.waw.pl",
                    wwwPage = "https://napaluchu.waw.pl/",
                    phone = "22 846 02 36",
                    createDate = LocalDateTime.now()
            )
    )

    fun shelter_dla_bezdomnych() = Shelter(
            name = "Schronisko Dla Bezdomnych Zwierząt w Józefowie",
            nip = 655467234,
            description = "Schronisko dla psów i kotów",
            createDate = LocalDateTime.now(),
            address = Address(
                    street = "Strużańska",
                    city = "Legionowo",
                    postalCode = "05-119",
                    buildingNumber = "15",
                    province = "Mazowieckie",
                    createDate = LocalDateTime.now()
            ),
            contact = Contact(
                    email = "",
                    wwwPage = "http://jozefow.eadopcje.org/",
                    phone = " biuro@schroniskojozefow.pl",
                    createDate = LocalDateTime.now()
            )
    )

    fun dog(number: Int, shelter: Shelter) = Dog(
            dogId = "32131-213321-1231-${number}",
            name = "Reksio $number",
            race = DogRace.values().random(),
            addedDate = LocalDateTime.now(),
            unregisteredDate = LocalDateTime.now(),
            description = "Zdrowy pies",
            imageDogUrl = "https://i2.wp.com/www.pies.pl/wp-content/uploads/pies-przekrzywia-glowe.jpg",
            shelter = shelter,
            checkedOut = false,
    )

    fun user(number: Int) = DogWebUser(
            // Hasło zahashowane algorytmem Bcrypt
            password = "\$2y\$10\$9W4QbzKS0qsGOQT1lBQ4zOnQZ.auPkeX8b7aXWTCSkars4QtoDbAO",
            email = "blacha${number}@wat.pl",
            createDate = LocalDateTime.now(),
            role = Role.ROLE_USER
    )
}
