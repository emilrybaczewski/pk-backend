package pl.edu.wat.dogwebbackend.service

import org.slf4j.LoggerFactory
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.stereotype.Service
import pl.edu.wat.dogwebbackend.model.*
import pl.edu.wat.dogwebbackend.repository.DogRepository
import pl.edu.wat.dogwebbackend.repository.ShelterRepository
import pl.edu.wat.dogwebbackend.repository.UserRepository
import java.time.LocalDateTime
import java.util.*
import kotlin.random.Random

@Service
class DogService(
    val dogRepository: DogRepository,
    val shelterRepository: ShelterRepository,
    val userRepository: UserRepository
) {
    val logger = LoggerFactory.getLogger(DogService::class.java)


    fun getAllDogs(): ResponseEntity<List<DogResponse>> {
        val dogsResponse = dogRepository.findAll().map {
            DogResponse(
                id = it.id,
                name = it.name,
                dogId = it.dogId,
                race = it.race,
                addedDate = it.addedDate,
                unregisteredDate = it.unregisteredDate,
                description = it.description,
                imageDogUrl = it.imageDogUrl,
                checkedOut = it.checkedOut,
                shelterId = it.shelter?.id,
                dogWebUserId = it.dogWebUser?.id
            )
        }
        return ResponseEntity(dogsResponse, HttpStatus.ACCEPTED)
    }

    fun getDogById(dogId: String): ResponseEntity<DogResponse> {
        val dog = dogRepository.findByDogId(dogId)
        if (dogId == dog?.dogId) {
            val dogsResponse =
                DogResponse(
                    id = dog.id,
                    name = dog.name,
                    dogId = dog.dogId,
                    race = dog.race,
                    addedDate = dog.addedDate,
                    unregisteredDate = dog.unregisteredDate,
                    description = dog.description,
                    imageDogUrl = dog.imageDogUrl,
                    checkedOut = dog.checkedOut,
                    shelterId = dog.shelter?.id,
                    dogWebUserId = dog.dogWebUser?.id
                )
            return ResponseEntity(dogsResponse, HttpStatus.ACCEPTED)
        }
        return ResponseEntity(null, HttpStatus.NOT_FOUND)
    }

    fun getRandomDog(): ResponseEntity<Dog?> {
        val count = dogRepository.count()

        if (count < 1) {
            return ResponseEntity(null, HttpStatus.SERVICE_UNAVAILABLE)
        }
        val randomId = Random.nextLong(1, count + 1)
        logger.info("zwrócono losowego psa")
        return ResponseEntity(dogRepository.getById(randomId), HttpStatus.ACCEPTED)
    }

    fun addDog(
        dogId: String,
        name: String,
        race: DogRace,
        shelter: Long,
        description: String,
        imageDogUrl: String,
        checkedOut: Boolean,
        dogWebUserId: Long
    ): ResponseEntity<StringResponse> {
        val shelterOptional = shelterRepository.findById(shelter)
        val dog = dogRepository.findByDogId(dogId)
        val owner = userRepository.findById(dogWebUserId)

        if (dogId == dog?.dogId) {
            logger.warn("Pies o z ID psa=$dogId już istnieje w tym schronisku")
            //return ResponseEntity(StringResponse("Pies o takim ID=$dogId juz istnieje w tym schronisku"), HttpStatus.CONFLICT)
            return ResponseEntity(
                StringResponse("Pies o takim ID=$dogId juz istnieje w tym schronisku"),
                HttpStatus.ACCEPTED
            )
        }
        if (shelterOptional.isPresent) {
            with(dogRepository) {
                save(
                    Dog(
                        dogId = dogId,
                        name = name,
                        race = race,
                        addedDate = LocalDateTime.now(),
                        unregisteredDate = null,
                        shelter = shelterOptional.get(),
                        description = description,
                        imageDogUrl = imageDogUrl,
                        checkedOut = checkedOut,
                        dogWebUser = owner.get()
                    )
                )
            }
            logger.warn("Udało się stworzyć nowego psa o dogId:$dogId")
            return ResponseEntity(StringResponse("Dodałeś psa do bazy"), HttpStatus.CREATED)
        }
        logger.warn("Schronisko o podanym id=$shelter nie istnieje!")
        //return ResponseEntity(StringResponse("Podane schronisko nie istnieje"), HttpStatus.NOT_FOUND)
        return ResponseEntity(StringResponse("Podane schronisko nie istnieje"), HttpStatus.ACCEPTED)
    }


    fun updateDog(
        id: Long,
        dogId: String,
        name: String,
        race: DogRace,
        unregisteredDate: LocalDateTime,
        description: String,
        imageDogUrl: String,
        checkedOut: Boolean,
        shelterId: Long,
        dogwebuserId: Long
    ): ResponseEntity<String> {
        val dogOptional = dogRepository.findById(id)
        val shelter = shelterRepository.findById(shelterId)
        val owner = userRepository.findById(dogwebuserId)

        if (shelter.isPresent) {
            if (owner.isPresent) {
                if (dogOptional.isPresent) {
                    val dog = dogOptional.get()

                    with(dogRepository) {
                        save(
                            Dog(
                                id = id,
                                dogId = dogId,
                                name = name,
                                race = race,
                                addedDate = dog.addedDate,
                                unregisteredDate = unregisteredDate,
                                shelter = shelter.get(),
                                description = description,
                                imageDogUrl = imageDogUrl,
                                checkedOut = checkedOut,
                                dogWebUser = owner.get()
                            )
                        )
                    }
                    logger.info("Zmieniono dane psa o id:$id")
                    return ResponseEntity("Zmieniłeś opis psa o id:$id", HttpStatus.ACCEPTED)
                }
                logger.warn("Nie ma psa o takim id:$id")
                return ResponseEntity("Nie ma psa o takim id:$id", HttpStatus.NOT_FOUND)
            }
            logger.warn("Nie ma użytkownika o takim webuserId:$dogwebuserId")
            return ResponseEntity("Nie ma takiego właściciela", HttpStatus.NOT_FOUND)
        }
        logger.warn("Nie ma schroniska o takim id:$shelterId")
        return ResponseEntity("Podane schronisko nie istnieje", HttpStatus.NOT_FOUND)
    }

}
