package pl.edu.wat.dogwebbackend.service

import org.slf4j.LoggerFactory
import org.springframework.stereotype.Service
import pl.edu.wat.dogwebbackend.model.ai.CategorizeRequest
import pl.edu.wat.dogwebbackend.repository.AiClient

@Service
class AiService(
    val aiClient: AiClient,
) {

    private val logger = LoggerFactory.getLogger(AiService::class.java)

    fun categorizeDogBreed(categorizeRequest: CategorizeRequest) {
        logger.info("Wywołano kategoryzację rasy psa...")
        val dogBreeds = aiClient.categorizeDogBreed(categorizeRequest)
        logger.info("Kategoryzacja rasy psa zakończona, rezultat=${dogBreeds.result.joinToString()}")
    }

}