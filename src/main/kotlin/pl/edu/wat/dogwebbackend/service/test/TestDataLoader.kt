package pl.edu.wat.dogwebbackend.service.test

import org.springframework.boot.CommandLineRunner
import org.springframework.stereotype.Component
import org.springframework.transaction.annotation.Transactional
import pl.edu.wat.dogwebbackend.repository.DogRepository
import pl.edu.wat.dogwebbackend.repository.ShelterRepository
import pl.edu.wat.dogwebbackend.repository.UserRepository

@Component
class TestDataLoader(
    val dogRepository: DogRepository,
    val userRepository: UserRepository,
    val shelterRepository: ShelterRepository
) : CommandLineRunner {

    @Transactional
    override fun run(vararg args: String?) {
        val shelters = listOf(
            shelterRepository.save(TestDataProvider.shelter_paluch()),
            shelterRepository.save(TestDataProvider.shelter_dla_bezdomnych())
        )

        repeat(10) {
            userRepository.save(TestDataProvider.user(it))
            dogRepository.save(TestDataProvider.dog(it, shelters.random()))
        }
    }
}
