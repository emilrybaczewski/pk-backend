package pl.edu.wat.dogwebbackend.service

import org.springframework.security.core.GrantedAuthority
import org.springframework.security.core.authority.SimpleGrantedAuthority
import org.springframework.security.core.userdetails.User
import org.springframework.security.core.userdetails.UserDetails
import org.springframework.security.core.userdetails.UserDetailsService
import org.springframework.security.core.userdetails.UsernameNotFoundException
import org.springframework.stereotype.Component
import pl.edu.wat.dogwebbackend.repository.AuthRepository
import java.util.*

@Component
class AppUserDetailsService(private val userRepository: AuthRepository) : UserDetailsService {

    @Throws(UsernameNotFoundException::class)
    override fun loadUserByUsername(email: String): UserDetails {
        val user = userRepository.findByEmail(email)
            .orElseThrow { UsernameNotFoundException("The username $email doesn't exist") }

        val authorities = ArrayList<GrantedAuthority>()

        // user.role.forEach { authorities.add(SimpleGrantedAuthority(it.Role)) } // TODO jak DogWebUser będzie miał listę ról

        authorities.add(SimpleGrantedAuthority(user.role.name))

        return User(
            user.email,
            user.password,
            authorities
        )
    }
}
