package pl.edu.wat.dogwebbackend.service

import org.slf4j.LoggerFactory
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.stereotype.Service
import pl.edu.wat.dogwebbackend.model.Address
import pl.edu.wat.dogwebbackend.model.Contact
import pl.edu.wat.dogwebbackend.model.Shelter
import pl.edu.wat.dogwebbackend.model.StringResponse
import pl.edu.wat.dogwebbackend.repository.ShelterAddressRepository
import pl.edu.wat.dogwebbackend.repository.ShelterContactRepository
import pl.edu.wat.dogwebbackend.repository.ShelterRepository
import java.time.LocalDateTime

@Service
class ShelterService(
        val shelterRepository: ShelterRepository,
        val shelterAddressRepository: ShelterAddressRepository,
        val shelterContactRepository: ShelterContactRepository,

        ) {
    val logger = LoggerFactory.getLogger(DogService::class.java)

    fun getAllShelters() = shelterRepository.findAll()


    fun createShelter(

            name: String,
            description: String,
            nip: Int,
    ): ResponseEntity<String> {
        with(shelterAddressRepository) {
            save(
                    Address(
                            street = "",
                            city = "",
                            postalCode = "",
                            buildingNumber = "",
                            province = "",
                            createDate = LocalDateTime.now(),

                            )
            )
        }
        with(shelterContactRepository) {
            save(
                    Contact(
                            email = "",
                            wwwPage = "",
                            phone = "",
                            createDate = LocalDateTime.now(),

                            )
            )
        }
        with(shelterRepository) {
            save(
                    Shelter(
                            name = name,
                            description = description,
                            createDate = LocalDateTime.now(),
                            nip = nip,
                    )
            )
        }
        logger.info("Udało się stworzyć nowe schronisko o nazwie:$name")
        return ResponseEntity("Utworzyłeś nowe schronisko o nazwie:$name", HttpStatus.CREATED)
    }

    fun updateShelter(
            id: Long,
            name: String,
            description: String,
            nip: Int,

            ): ResponseEntity<String> {
        val shelterOptional = shelterRepository.findById(id)

        if (shelterOptional.isPresent) {
            val shelter = shelterOptional.get()
            with(shelterRepository) {
                save(
                        Shelter(
                                id = id,
                                name = name,
                                description = description,
                                createDate = shelter.createDate,
                                nip = nip,
                                address = shelter.address,
                                contact = shelter.contact

                        )
                )
            }
            logger.info("Zmieniono dane schroniska o id:$id")
            return ResponseEntity("Zmieniłeś opis schroniska", HttpStatus.ACCEPTED)
        }
        logger.warn("Nie ma schroniska o takim id:$id")
        //return ResponseEntity("Podane schronisko nie istnieje", HttpStatus.NOT_FOUND)
        return ResponseEntity("Podane schronisko nie istnieje", HttpStatus.ACCEPTED)
    }


    fun updateShelterAddress(
            id: Long,
            street: String,
            city: String,
            postalCode: String,
            buildingNumber: String,
            province: String

    ): ResponseEntity<StringResponse> {
        val shelterOptional = shelterRepository.findById(id)

        if (shelterOptional.isPresent) {
            val shelter = shelterOptional.get()
            val addressid = shelter.address?.id
            if (addressid != null) {
                val addressOptional = shelterAddressRepository.findById(addressid)
                val address = addressOptional.get()
                with(shelterAddressRepository) {
                    save(
                            Address(
                                    id = addressid,
                                    street = street,
                                    city = city,
                                    postalCode = postalCode,
                                    buildingNumber = buildingNumber,
                                    province = province,
                                    createDate = address.createDate,

                                    )
                    )
                }

            }
            logger.info("Zmieniono adres schroniska o id:$id")
            return ResponseEntity(StringResponse("Zmieniłeś adres schroniska"), HttpStatus.ACCEPTED)

        }

        logger.warn("Nie ma schroniska o takim id:$id")
        //return ResponseEntity("Nie ma takiego schroniska", HttpStatus.NOT_FOUND)
        return ResponseEntity(StringResponse("Nie ma takiego schroniska"), HttpStatus.ACCEPTED)
    }

    fun updateShelterContact(
            id: Long,
            email: String,
            wwwPage: String,
            phone: String,

            ): ResponseEntity<String> {
        val shelterOptional = shelterRepository.findById(id)

        if (shelterOptional.isPresent) {
            val shelter = shelterOptional.get()
            val contactid = shelter.contact?.id
            if (contactid != null) {
                val contactOptional = shelterContactRepository.findById(contactid)
                val contact = contactOptional.get()
                with(shelterContactRepository) {
                    save(
                            Contact(
                                    id = contactid,
                                    email = email,
                                    wwwPage = wwwPage,
                                    phone = phone,
                                    createDate = contact.createDate,

                                    )
                    )
                }

            }
            logger.info("Zmieniono dane kontaktowe schroniska o id:$id")
            return ResponseEntity("Zmieniłeś dane kontaktowe schroniska", HttpStatus.ACCEPTED)

        }

        logger.warn("Nie ma schroniska o takim id:$id")
        //return ResponseEntity("Nie ma takiego schroniska", HttpStatus.NOT_FOUND)
        return ResponseEntity("Nie ma takiego schroniska", HttpStatus.ACCEPTED)
    }

}
