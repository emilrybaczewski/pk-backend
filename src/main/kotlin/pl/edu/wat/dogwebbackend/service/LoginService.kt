package pl.edu.wat.dogwebbackend.service


import io.jsonwebtoken.Jwts
import io.jsonwebtoken.SignatureAlgorithm
import io.jsonwebtoken.security.Keys
import org.slf4j.LoggerFactory
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.security.authentication.AuthenticationManager
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.security.core.Authentication
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.security.core.userdetails.User
import org.springframework.stereotype.Service
import pl.edu.wat.dogwebbackend.config.SecurityProperties
import pl.edu.wat.dogwebbackend.model.DogWebUserResponse
import pl.edu.wat.dogwebbackend.model.LoginResponse
import pl.edu.wat.dogwebbackend.repository.UserRepository
import java.time.LocalDateTime
import java.time.ZoneId
import java.util.*

@Service
class LoginService(
        val authenticationManager: AuthenticationManager,
        val securityProperties: SecurityProperties,
        val userRepository: UserRepository
) {
    val logger = LoggerFactory.getLogger(LoginService::class.java)

    fun login(email: String, password: String): ResponseEntity<LoginResponse?> {
        return try {
            // Loguje usera mechanizmem Springa
            val auth = authenticationManager.authenticate(
                    UsernamePasswordAuthenticationToken(
                            email,
                            password,
                            ArrayList()
                    )
            )
            val userOptional = userRepository.findByEmail(email)
            val userInBase = userOptional.get()
            val user: DogWebUserResponse = DogWebUserResponse(
                    id = userInBase.id,
                    email = userInBase.email,
                    password = "",
                    createDate = userInBase.createDate,
                    role = userInBase.role,
                    shelterId = userInBase.shelter?.id)


            val jwtToken = generateJwtToken(auth)
            val loginResponse = LoginResponse(jwtToken, user)
            logger.info("Logowanie powiodło się. Uwierzytelnianie użytkownika:$email")
            ResponseEntity(loginResponse, HttpStatus.ACCEPTED)
        } catch (e: Exception) { // TODO - można spróbować rozbić na bardziej sprecyzowane wyjątki - patrz metodę authenticationManager.authenticate(#)
            logger.info("Logowanie nie powiodło się! Email=$email.")
            ResponseEntity(null, HttpStatus.BAD_REQUEST)
        }

    }

    fun refreshToken(): String {
        val authentication = SecurityContextHolder.getContext().authentication
        logger.info("Generuje nowy token dla użytkownika=${authentication.name}")
        return generateJwtToken(authentication)
    }

    private fun generateJwtToken(auth: Authentication): String {
        val authClaims: MutableList<String> = mutableListOf()

        auth.authorities?.let {
            it.forEach { claim -> authClaims.add(claim.toString()) }
        }

        // TODO zbadać dlaczego po zalogowaniu się w auth.principal nie jest już User tylko username
        val username: String = if (auth.principal is User) (auth.principal as User).username else auth.principal as String

        val token = Jwts.builder()
                .setSubject(username)
                .claim("auth", authClaims)
                .setExpiration(
                        Date.from(
                                LocalDateTime.now().plusMinutes(securityProperties.expirationTime.toLong())
                                        .atZone(ZoneId.systemDefault())
                                        .toInstant()
                        )

                )
                .signWith(Keys.hmacShaKeyFor(securityProperties.secret.toByteArray()), SignatureAlgorithm.HS512)
                .compact()

        return securityProperties.tokenPrefix + token
    }
}


