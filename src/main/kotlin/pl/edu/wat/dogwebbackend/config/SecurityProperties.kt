package pl.edu.wat.dogwebbackend.config

import org.springframework.boot.context.properties.ConfigurationProperties

/**
 * Klasa z ustawieniami security - można by przenieść te opcje do pliku.
 *
 * Mamy tutaj, że każdy request (żądanie do serwera), aby był uwierzytelniony to w HEADERze HTTP musi być klucz o nazwie
 * Authorization, który musi zawierać token JWT. UWAGA! Token musi być poprzedzony prefixem "Bearer " <- tak, jest tam spacja!
 */
@ConfigurationProperties(prefix = "jwt-security")
data class SecurityProperties(
    /**
     * Minimum length for the secret is 42.
     */
    val secret: String = "PseudoSecret-Pseudosecret-Please-Use-Ur-Own-Key-PseudoSecret-Pseudosecret",
    val expirationTime: Int = 30, // in minutes
    val tokenPrefix: String = "Bearer ",
    val headerString: String = "Authorization",
    val strength: Int = 10
)
