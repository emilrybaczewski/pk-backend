package pl.edu.wat.dogwebbackend.config

import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.security.authentication.AuthenticationManager
import org.springframework.security.authentication.dao.DaoAuthenticationProvider
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity
import org.springframework.security.config.annotation.web.builders.HttpSecurity
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter
import org.springframework.security.config.http.SessionCreationPolicy
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder
import pl.edu.wat.dogwebbackend.security.JWTAuthorizationFilter
import pl.edu.wat.dogwebbackend.service.AppUserDetailsService

@Configuration
@EnableGlobalMethodSecurity(prePostEnabled = true)
class WebConfig(
    val bCryptPasswordEncoder: BCryptPasswordEncoder,
    val userDetailsService: AppUserDetailsService,
    val securityProperties: SecurityProperties
) : WebSecurityConfigurerAdapter() {

    override fun configure(http: HttpSecurity) {
        http
            .cors().and()
            .csrf().disable()
            .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS) // no sessions
            .and()
            .authorizeRequests()
            // Pozwalam bez autoryzacji na te requesty
            .antMatchers("/", "/login/**", "/error/**", "/user/add", "/Ai/**", "/dogs/all", "/dogs/categorize").permitAll()
            // Pozwalam bez autoryzacji na swaggera
            .antMatchers("/swagger-ui/**", "/swagger-ui.html", "/webjars/**", "/v2/**", "/swagger-resources/**").permitAll()
            // Resztę requestów blokuje
            .anyRequest().authenticated()
            .and()
            // Każde żądanie przejdzie przez sprawdzenie JWT Tokena w tym filtrze
            .addFilter(JWTAuthorizationFilter(provideAuthenticationManager(), securityProperties))
    }

    @Throws(Exception::class)
    override fun configure(auth: AuthenticationManagerBuilder) {
        auth
            .userDetailsService(userDetailsService)
            .passwordEncoder(bCryptPasswordEncoder)
    }

    @Bean
    fun authProvider(): DaoAuthenticationProvider = DaoAuthenticationProvider().apply {
        setUserDetailsService(userDetailsService)
        setPasswordEncoder(bCryptPasswordEncoder)
    }

    @Bean
    fun provideAuthenticationManager(): AuthenticationManager = super.authenticationManager()

}
