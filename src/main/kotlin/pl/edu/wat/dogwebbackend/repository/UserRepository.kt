package pl.edu.wat.dogwebbackend.repository

import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.repository.CrudRepository
import org.springframework.stereotype.Repository
import pl.edu.wat.dogwebbackend.model.DogWebUser
import java.util.*

@Repository
interface UserRepository : JpaRepository<DogWebUser, Long> {

    fun findByEmail(email: String): Optional<DogWebUser>
}

@Repository
interface  AuthRepository : CrudRepository<DogWebUser, Long>{
    fun findByEmail(email: String): Optional<DogWebUser>
}

