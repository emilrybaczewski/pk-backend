package pl.edu.wat.dogwebbackend.repository

import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository
import pl.edu.wat.dogwebbackend.model.Contact


@Repository
interface ShelterContactRepository : JpaRepository<Contact, Long>