package pl.edu.wat.dogwebbackend.repository

import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query
import org.springframework.stereotype.Repository
import pl.edu.wat.dogwebbackend.model.Dog

@Repository
interface DogRepository : JpaRepository<Dog, Long> {
    fun findByDogId(dogId: String): Dog?

    @Query("SELECT dogs FROM Dog dogs WHERE dogs.checkedOut = :checkedOut")
    fun findAll(checkedOut: Boolean): List<Dog>

    fun findDogById(dogID: Long): Dog?

}
