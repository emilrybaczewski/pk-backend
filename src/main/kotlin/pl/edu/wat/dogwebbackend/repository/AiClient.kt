package pl.edu.wat.dogwebbackend.repository

import org.springframework.cloud.openfeign.FeignClient
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestMethod
import pl.edu.wat.dogwebbackend.model.ai.CategorizeRequest
import pl.edu.wat.dogwebbackend.model.ai.CategorizeResponse

@FeignClient(name = "dogweb-ai", url = "http://localhost:5000/api/v1")
interface AiClient {

    @RequestMapping(value = ["/categorize"], method = [RequestMethod.POST], consumes = ["application/json"])
    fun categorizeDogBreed(@RequestBody categorizeRequest: CategorizeRequest): CategorizeResponse

}