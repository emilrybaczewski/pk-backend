package pl.edu.wat.dogwebbackend.model.ai

data class CategorizeResponse(
    val result: List<PredictionResponse>
)
