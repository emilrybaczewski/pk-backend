package pl.edu.wat.dogwebbackend.model

import com.fasterxml.jackson.annotation.JsonBackReference
import java.time.LocalDateTime
import javax.persistence.*

@Entity
data class Shelter(
        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        val id: Long? = null,

        val name: String,
        val description: String,
        val createDate: LocalDateTime,
        val nip: Int,

        @OneToOne(cascade = [CascadeType.ALL])
        @JoinColumn(name = "address_id", referencedColumnName = "id")
        val address: Address? = null,

        @OneToOne(cascade = [CascadeType.ALL])
        @JoinColumn(name = "contact_id", referencedColumnName = "id")
        val contact: Contact? = null,
) {
    @OneToMany(mappedBy = "shelter")
    val dogWebUser: List<DogWebUser>? = null

    @JsonBackReference
    @OneToMany(mappedBy = "shelter")
    val dog: List<Dog>? = null
}
