package pl.edu.wat.dogwebbackend.model

data class GetDogByIdRequest (
    val dogId: String
)