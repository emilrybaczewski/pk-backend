package pl.edu.wat.dogwebbackend.model

import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id

enum class Role {
    ROLE_USER,
    ROLE_EMPLOYEE,
    ROLE_ADMIN,
    ROLE_SUPER_ADMIN
}