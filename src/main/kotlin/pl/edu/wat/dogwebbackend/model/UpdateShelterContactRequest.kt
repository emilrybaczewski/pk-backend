package pl.edu.wat.dogwebbackend.model

data class UpdateShelterContactRequest(

        val id: Long,
        val email: String,
        val wwwPage: String,
        val phone: String,
)
