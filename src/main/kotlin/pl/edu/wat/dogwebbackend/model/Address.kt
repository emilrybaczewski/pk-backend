package pl.edu.wat.dogwebbackend.model

import com.fasterxml.jackson.annotation.JsonBackReference
import java.time.LocalDateTime
import javax.persistence.*

@Entity
data class Address(
        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        val id: Long? = null,

        val street: String,
        val city: String,
        val postalCode: String,
        val buildingNumber: String,
        val province: String,
        val createDate: LocalDateTime,
) {
        @JsonBackReference
        @OneToOne(mappedBy = "address")
        val shelter: Shelter? = null
}
