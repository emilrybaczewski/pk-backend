package pl.edu.wat.dogwebbackend.model

data class UpdateUserPasswordRequest(
        val email: String,
        val password: String,

        )
