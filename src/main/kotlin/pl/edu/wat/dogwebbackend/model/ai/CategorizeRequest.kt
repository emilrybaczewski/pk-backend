package pl.edu.wat.dogwebbackend.model.ai

data class CategorizeRequest(
    val img: String
)
