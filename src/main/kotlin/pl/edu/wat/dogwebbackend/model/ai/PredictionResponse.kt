package pl.edu.wat.dogwebbackend.model.ai

data class PredictionResponse(
    val breed: String,
    val prediction_value: Double
)