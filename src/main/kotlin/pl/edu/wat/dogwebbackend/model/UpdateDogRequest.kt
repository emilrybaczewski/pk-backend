package pl.edu.wat.dogwebbackend.model

import java.time.LocalDateTime

data class UpdateDogRequest(
        val id: Long,
        val dogId: String,
        val name: String,
        val race: DogRace,
        val unregisteredDate: LocalDateTime,
        val description: String,
        val imageDogUrl: String,
        val checkedOut: Boolean,
        val shelterId: Long,
        val dogwebuserId: Long
)



