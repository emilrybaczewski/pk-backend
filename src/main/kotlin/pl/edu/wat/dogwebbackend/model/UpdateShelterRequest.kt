package pl.edu.wat.dogwebbackend.model

data class UpdateShelterRequest(
        val id: Long,
        val name: String,
        val description: String,
        val nip: Int


)
