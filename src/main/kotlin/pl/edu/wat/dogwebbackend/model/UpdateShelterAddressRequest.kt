package pl.edu.wat.dogwebbackend.model

data class UpdateShelterAddressRequest(

        val id: Long,
        val street: String,
        val city: String,
        val postalCode: String,
        val buildingNumber: String,
        val province: String,
)
