package pl.edu.wat.dogwebbackend.model

data class UpdateUserRoleRequest(
        val email: String,
        val role: Role

)
