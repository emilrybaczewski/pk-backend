package pl.edu.wat.dogwebbackend.model

data class RegisterUserRequest(

        val email: String,
        val password: String,
        val shelterId: Long
)


