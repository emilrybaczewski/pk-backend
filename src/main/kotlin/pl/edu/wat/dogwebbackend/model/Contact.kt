package pl.edu.wat.dogwebbackend.model

import com.fasterxml.jackson.annotation.JsonBackReference
import java.time.LocalDateTime
import javax.persistence.*

@Entity
data class Contact(
        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        val id: Long? = null,

        val email: String,
        val wwwPage: String,
        val phone: String,
        val createDate: LocalDateTime,
) {
        @JsonBackReference
        @OneToOne(mappedBy = "contact")
        val shelter: Shelter? = null
}
