package pl.edu.wat.dogwebbackend.model

import java.time.LocalDateTime

data class DogResponse (
        val id:Long?,
        val dogId: String,
        val name: String,
        val race: DogRace,
        val addedDate: LocalDateTime,
        val unregisteredDate: LocalDateTime? = null,
        val description: String,
        val imageDogUrl: String,
        val checkedOut: Boolean = false,
        val shelterId: Long?,
        val dogWebUserId: Long?
        )