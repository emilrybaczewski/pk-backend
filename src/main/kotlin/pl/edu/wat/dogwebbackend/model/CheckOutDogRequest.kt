package pl.edu.wat.dogwebbackend.model

import java.time.LocalDateTime

data class CheckOutDogRequest(
        val id: Long,
)
