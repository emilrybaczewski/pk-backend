package pl.edu.wat.dogwebbackend.model

data class AddDogRequest(
    val dogId: String,
    val name: String,
    val race: DogRace,
    val shelterId: Long,
    val description: String,
    val imageDogUrl: String,
    val checkedOut: Boolean,
    val dogWebUserID: Long
)
