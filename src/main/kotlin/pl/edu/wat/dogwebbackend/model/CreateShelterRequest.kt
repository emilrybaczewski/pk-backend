package pl.edu.wat.dogwebbackend.model

data class CreateShelterRequest(

        val name: String,
        val description: String,
        val nip: Int
)
