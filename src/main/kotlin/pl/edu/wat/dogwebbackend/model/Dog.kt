package pl.edu.wat.dogwebbackend.model

import com.fasterxml.jackson.annotation.JsonManagedReference
import java.time.LocalDateTime
import javax.persistence.*

@Entity
data class Dog(
        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        val id: Long? = null,

        val dogId: String,
        val name: String,
        @Enumerated(EnumType.STRING)
        val race: DogRace,
        val addedDate: LocalDateTime,
        val unregisteredDate: LocalDateTime? = null,
        val description: String,
        val imageDogUrl: String,
        val checkedOut: Boolean = false,

        @JsonManagedReference
        @ManyToOne(cascade = [CascadeType.ALL])
        val shelter: Shelter? = null,

        @JsonManagedReference
        @ManyToOne(cascade = [CascadeType.ALL])
        val dogWebUser: DogWebUser? = null
)
