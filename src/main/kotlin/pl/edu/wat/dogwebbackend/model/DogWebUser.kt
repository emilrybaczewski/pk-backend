package pl.edu.wat.dogwebbackend.model

import com.fasterxml.jackson.annotation.JsonBackReference
import java.time.LocalDateTime
import javax.persistence.*

@Entity
data class DogWebUser(
        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        val id: Long? = null,

        @Column(unique = true)
        val email: String,
        var password: String,
        val createDate: LocalDateTime,
        @Enumerated(EnumType.STRING)
        val role: Role,
        @ManyToOne(cascade = [CascadeType.ALL])
        val shelter: Shelter? = null

) {
        @JsonBackReference
        @OneToMany(mappedBy = "dogWebUser")
        val dog: List<Dog>? = null


}
