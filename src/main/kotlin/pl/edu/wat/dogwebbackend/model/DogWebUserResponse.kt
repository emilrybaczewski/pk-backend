package pl.edu.wat.dogwebbackend.model

import java.time.LocalDateTime
import javax.persistence.*

data class DogWebUserResponse (

        val id: Long?,
        val email: String,
        val password: String,
        val createDate: LocalDateTime,
        val role: Role,
        val shelterId: Long?
        )