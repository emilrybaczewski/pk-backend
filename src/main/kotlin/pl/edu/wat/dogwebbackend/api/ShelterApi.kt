package pl.edu.wat.dogwebbackend.api

import io.swagger.annotations.ApiOperation
import io.swagger.annotations.Authorization
import org.slf4j.LoggerFactory
import org.springframework.http.ResponseEntity
import org.springframework.security.access.prepost.PreAuthorize
import org.springframework.web.bind.annotation.*
import pl.edu.wat.dogwebbackend.config.SwaggerConfig
import pl.edu.wat.dogwebbackend.model.*
import pl.edu.wat.dogwebbackend.service.ShelterService


@RestController
@RequestMapping("/shelter")
class ShelterApi
(
        val shelterService: ShelterService
) {
    val logger = LoggerFactory.getLogger(DogApi::class.java)


    @GetMapping("/all")
    @PreAuthorize("hasAnyAuthority('ROLE_USER', 'ROLE_EMPLOYEE', 'ROLE_ADMIN', 'ROLE_SUPER_ADMIN')")
    @ApiOperation(value = "Pobieranie listy wszystkich schronisk", response = Shelter::class, responseContainer = "List", authorizations = [Authorization(SwaggerConfig.JWT_TOKEN_API_KEY_NAME)])
    fun getAllDogsgetAllShelters(): MutableList<Shelter> {
        logger.info("Próba pobrania wszystkich schronisk")
        val shelter = shelterService.getAllShelters()
        logger.info("Pobrano wszystkie schronika. Liczba psów=${shelter.size}")
        return shelter
    }

    @PostMapping("/create")
    @PreAuthorize("hasAnyAuthority('ROLE_USER', 'ROLE_EMPLOYEE', 'ROLE_ADMIN', 'ROLE_SUPER_ADMIN')")
    @ApiOperation(value = "Dodawanie nowego schroniska", response = String::class, authorizations = [Authorization(SwaggerConfig.JWT_TOKEN_API_KEY_NAME)])
    fun createShelter(@RequestBody createShelterRequest: CreateShelterRequest): ResponseEntity<String>{
        return shelterService.createShelter(
                createShelterRequest.name,
                createShelterRequest.description,
                createShelterRequest.nip

        )
    }


    @PutMapping("/update")
    @PreAuthorize("hasAnyAuthority('ROLE_USER', 'ROLE_EMPLOYEE', 'ROLE_ADMIN', 'ROLE_SUPER_ADMIN')")
    @ApiOperation(value = "Aktualizowanie schroniska", response = String::class, authorizations = [Authorization(SwaggerConfig.JWT_TOKEN_API_KEY_NAME)])
    fun updateShelter(@RequestBody updateShelterRequest: UpdateShelterRequest): ResponseEntity<String> {
        return shelterService.updateShelter(
                updateShelterRequest.id,
                updateShelterRequest.name,
                updateShelterRequest.description,
                updateShelterRequest.nip

        )
    }


    @PutMapping("/updateaddress")
    @PreAuthorize("hasAnyAuthority('ROLE_USER', 'ROLE_EMPLOYEE', 'ROLE_ADMIN', 'ROLE_SUPER_ADMIN')")
    @ApiOperation(value = "Aktualizowanie adresu schroniska", response = StringResponse::class, authorizations = [Authorization(SwaggerConfig.JWT_TOKEN_API_KEY_NAME)])
    fun updateShelter(@RequestBody updateShelterAddressRequest: UpdateShelterAddressRequest): ResponseEntity<StringResponse> {
        return shelterService.updateShelterAddress(
                updateShelterAddressRequest.id,
                updateShelterAddressRequest.city,
                updateShelterAddressRequest.street,
                updateShelterAddressRequest.buildingNumber,
                updateShelterAddressRequest.postalCode,
                updateShelterAddressRequest.province,
        )
    }

    @PutMapping("/updatecontact")
    @PreAuthorize("hasAnyAuthority('ROLE_USER', 'ROLE_EMPLOYEE', 'ROLE_ADMIN', 'ROLE_SUPER_ADMIN')")
    @ApiOperation(value = "Aktualizowanie kontaktu schroniska", response = String::class, authorizations = [Authorization(SwaggerConfig.JWT_TOKEN_API_KEY_NAME)])
    fun updateShelter(@RequestBody updateShelterContactRequest: UpdateShelterContactRequest): ResponseEntity<String> {
        return shelterService.updateShelterContact(
                updateShelterContactRequest.id,
                updateShelterContactRequest.email,
                updateShelterContactRequest.wwwPage,
                updateShelterContactRequest.phone,
        )
    }

}

