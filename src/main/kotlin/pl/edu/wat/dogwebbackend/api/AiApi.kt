package pl.edu.wat.dogwebbackend.api

import io.swagger.annotations.ApiOperation
import io.swagger.annotations.Authorization
import org.springframework.security.access.prepost.PreAuthorize
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import pl.edu.wat.dogwebbackend.config.SwaggerConfig
import pl.edu.wat.dogwebbackend.model.ai.CategorizeRequest
import pl.edu.wat.dogwebbackend.service.AiService

@RestController
@RequestMapping("/dogs")
class AiApi(
    val aiService: AiService,
) {

    @PostMapping("/categorize")
//    @PreAuthorize("hasAnyAuthority('ROLE_USER', 'ROLE_EMPLOYEE', 'ROLE_ADMIN', 'ROLE_SUPER_ADMIN')")
//    @ApiOperation(value = "Kategoryzacja rasy psa siecią neuronową", response = String::class, authorizations = [Authorization(SwaggerConfig.JWT_TOKEN_API_KEY_NAME)])
    fun categorizeDogBreed(@RequestBody aiRequest: CategorizeRequest): String {
        aiService.categorizeDogBreed(aiRequest)
        return "OK"
    }

}
