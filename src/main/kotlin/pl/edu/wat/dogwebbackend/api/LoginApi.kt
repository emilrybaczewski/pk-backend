package pl.edu.wat.dogwebbackend.api

import io.swagger.annotations.ApiOperation
import io.swagger.annotations.Authorization
import org.springframework.http.ResponseEntity
import org.springframework.security.access.prepost.PreAuthorize
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import pl.edu.wat.dogwebbackend.config.SwaggerConfig
import pl.edu.wat.dogwebbackend.model.Dog
import pl.edu.wat.dogwebbackend.model.LoginRequest
import pl.edu.wat.dogwebbackend.model.LoginResponse
import pl.edu.wat.dogwebbackend.service.LoginService


@RestController
@RequestMapping("/login")
class LoginApi(
        val loginService: LoginService
) {

    @PostMapping
    fun login(@RequestBody loginRequest: LoginRequest): ResponseEntity<LoginResponse?> {
        return loginService.login(loginRequest.email, loginRequest.password)
    }

    @PreAuthorize("hasAnyAuthority('ROLE_USER', 'ROLE_EMPLOYEE', 'ROLE_ADMIN', 'ROLE_SUPER_ADMIN')")
    @ApiOperation(value = "Odświeżanie tokenu JWT dla zalogowanego użytkownika", response = String::class, authorizations = [Authorization(SwaggerConfig.JWT_TOKEN_API_KEY_NAME)])
    @PostMapping("/refresh-token")
    fun refreshToken(): String {
        return loginService.refreshToken()
    }

}