package pl.edu.wat.dogwebbackend.api

import io.swagger.annotations.ApiOperation
import io.swagger.annotations.Authorization
import org.springframework.security.access.prepost.PreAuthorize
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import pl.edu.wat.dogwebbackend.config.SwaggerConfig
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import pl.edu.wat.dogwebbackend.model.*
import pl.edu.wat.dogwebbackend.service.UserService

@RestController
@RequestMapping("/user")
class UserApi(
        val UserService: UserService
) {
    @PostMapping("/add")
    fun addUser(@RequestBody registerUserRequest: RegisterUserRequest): ResponseEntity<StringResponse> {
        return UserService.addUser(
                registerUserRequest.email,
                registerUserRequest.password,
                registerUserRequest.shelterId
        )
    }
    @PostMapping("/password")
    @PreAuthorize("hasAnyAuthority('ROLE_USER', 'ROLE_EMPLOYEE', 'ROLE_ADMIN', 'ROLE_SUPER_ADMIN')")
    @ApiOperation(value = "Zmiana hasła użytkownika", response = String::class, authorizations = [Authorization(SwaggerConfig.JWT_TOKEN_API_KEY_NAME)])
    fun updateUserPassword(@RequestBody updateUserPasswordRequest: UpdateUserPasswordRequest): ResponseEntity<String> {
        return UserService.updateUserPassword(
                updateUserPasswordRequest.email,
                updateUserPasswordRequest.password
        )
    }

    @PostMapping("/role")
    @PreAuthorize("hasAnyAuthority('ROLE_USER', 'ROLE_EMPLOYEE', 'ROLE_ADMIN', 'ROLE_SUPER_ADMIN')")
    @ApiOperation(value = "Zmiana roli użytkownika", response = String::class, authorizations = [Authorization(SwaggerConfig.JWT_TOKEN_API_KEY_NAME)])
    fun updateuserrole(@RequestBody updateUserRoleRequest: UpdateUserRoleRequest): ResponseEntity<String> {
        return UserService.updateUserRole(
                updateUserRoleRequest.email,
                updateUserRoleRequest.role,
        )
    }

    @GetMapping("/all")
    @PreAuthorize("hasAnyAuthority('ROLE_USER', 'ROLE_EMPLOYEE', 'ROLE_ADMIN', 'ROLE_SUPER_ADMIN')")
    @ApiOperation(value = "Pobieranie listy wszystkich użytkowników serwisu", response = DogWebUser::class, responseContainer = "List", authorizations = [Authorization(
        SwaggerConfig.JWT_TOKEN_API_KEY_NAME
    )])
    fun getUsers(): MutableList<DogWebUser> {
        val users = UserService.getUsers()
        return users
    }

    @GetMapping("/random")
    @PreAuthorize("hasAnyAuthority('ROLE_USER', 'ROLE_EMPLOYEE', 'ROLE_ADMIN', 'ROLE_SUPER_ADMIN')")
    @ApiOperation(value = "Pobieranie losowego użytkownika", response = DogWebUser::class, authorizations = [Authorization(SwaggerConfig.JWT_TOKEN_API_KEY_NAME)])
    fun getUser() = UserService.getUser()

//    @GetMapping("/uid")
//    fun getUserid(@RequestParam("id") id: Long) {
//        return getUserid(id)
//    }

}
