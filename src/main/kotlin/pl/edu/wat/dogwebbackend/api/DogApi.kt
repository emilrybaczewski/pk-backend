package pl.edu.wat.dogwebbackend.api

import io.swagger.annotations.ApiOperation
import io.swagger.annotations.Authorization
import org.slf4j.LoggerFactory
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.security.access.prepost.PreAuthorize
import org.springframework.web.bind.annotation.*
import pl.edu.wat.dogwebbackend.config.SwaggerConfig.Companion.JWT_TOKEN_API_KEY_NAME
import pl.edu.wat.dogwebbackend.model.*
import pl.edu.wat.dogwebbackend.service.DogService


@RestController
@RequestMapping("/dogs")
class DogApi(
        val dogService: DogService
) {

    val logger = LoggerFactory.getLogger(DogApi::class.java)

    @GetMapping("/all")
//    @PreAuthorize("hasAnyAuthority('ROLE_USER', 'ROLE_EMPLOYEE', 'ROLE_ADMIN', 'ROLE_SUPER_ADMIN')")
//    @ApiOperation(value = "Pobieranie listy wszystkich psów", response = Dog::class, responseContainer = "List", authorizations = [Authorization(JWT_TOKEN_API_KEY_NAME)])
    fun getAllDogs(): ResponseEntity<List<DogResponse>> {
        logger.info("Próba pobrania wszystkich psów")
        val response = dogService.getAllDogs()
        logger.info("Pobrano wszystkie psy. Liczba psów=${response.body?.size}")
        return response
    }


    @PostMapping("/add-Dog")
    @PreAuthorize("hasAnyAuthority('ROLE_USER', 'ROLE_EMPLOYEE', 'ROLE_ADMIN', 'ROLE_SUPER_ADMIN')")
    @ApiOperation(value = "Dodawanie nowego psa", response = StringResponse::class, authorizations = [Authorization(JWT_TOKEN_API_KEY_NAME)])
    fun addDog(@RequestBody addDogRequest: AddDogRequest): ResponseEntity<StringResponse>{
        return dogService.addDog(
                addDogRequest.dogId,
                addDogRequest.name,
                addDogRequest.race,
                addDogRequest.shelterId,
                addDogRequest.description,
                addDogRequest.imageDogUrl,
                addDogRequest.checkedOut,
                addDogRequest.dogWebUserID
        )
    }

    @GetMapping("/random")
    @PreAuthorize("hasAnyAuthority('ROLE_USER', 'ROLE_EMPLOYEE', 'ROLE_ADMIN', 'ROLE_SUPER_ADMIN')")
    @ApiOperation(value = "Pobieranie losowego psa", response = Dog::class, authorizations = [Authorization(JWT_TOKEN_API_KEY_NAME)])
    fun getRandomDog() = dogService.getRandomDog()


    @GetMapping("/get-Dog")
    fun getDog(@RequestBody getDogByIdRequest: GetDogByIdRequest): ResponseEntity<DogResponse> {
        return dogService.getDogById(getDogByIdRequest.dogId)
    }

    @PutMapping("/update")
    @PreAuthorize("hasAnyAuthority('ROLE_USER', 'ROLE_EMPLOYEE', 'ROLE_ADMIN', 'ROLE_SUPER_ADMIN')")
    @ApiOperation(value = "Aktualizowanie psa", response = String::class, authorizations = [Authorization(JWT_TOKEN_API_KEY_NAME)])
    fun updateDog(@RequestBody upDateDogRequest: UpdateDogRequest): ResponseEntity<String> {
        return dogService.updateDog(
                upDateDogRequest.id,
                upDateDogRequest.dogId,
                upDateDogRequest.name,
                upDateDogRequest.race,
                upDateDogRequest.unregisteredDate,
                upDateDogRequest.description,
                upDateDogRequest.imageDogUrl,
                upDateDogRequest.checkedOut,
                upDateDogRequest.shelterId,
                upDateDogRequest.dogwebuserId,
        )
    }
}
