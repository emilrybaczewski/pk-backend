package pl.edu.wat.dogwebbackend

import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RestController

@RestController
class HelloWorldApi {

    @GetMapping("/")
    fun helloWorld() = "<html>\n" + "<header><title>DogWeb</title></header>\n" +
            "<body>\nWitaj w systemie DogWeb <br> <br> <a href=\"/swagger-ui/index.html\">Dokumentacja API w Swaggerze</a>" +
            "</body>\n" + "</html>";

}
